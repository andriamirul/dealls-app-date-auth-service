package api

import (
	"context"
	"dealls-app-date-auth-service/server/pb"
	"errors"
	"net/http"
	"strconv"

	"go.elastic.co/apm/v2"
)

func (s *Server) PackageList(ctx context.Context, req *pb.Empty) (*pb.PackageListResponse, error) {
	span, _ := apm.StartSpan(ctx, "PackageList", "process")
	span.Action = "execute"
	defer span.End()

	logger := s.GetLogger()
	logger.Infof("[func: register] Check request %v", req)

	accessToken, err := s.manager.GetAccessToken(ctx)
	if err != nil {
		return nil, err
	}

	_, err = s.manager.Verify(*accessToken)
	if err != nil {
		return nil, err
	}

	packageList, err := s.provider.GetPackageList(ctx)
	if err != nil {
		return nil, err
	}

	if len(packageList) == 0 {
		return nil, errors.New("We currently doesn't have any purchasing offer")
	}

	return &pb.PackageListResponse{
		Error:   false,
		Code:    http.StatusOK,
		Message: "Success",
		Package: packageList,
	}, nil
}

func (s *Server) PurchasePackage(ctx context.Context, req *pb.PurchasePackageRequest) (*pb.DefaultResponse, error) {
	span, _ := apm.StartSpan(ctx, "PurchasePackage", "process")
	span.Action = "execute"
	defer span.End()

	logger := s.GetLogger()
	logger.Infof("[func: register] Check request %v", req)

	accessToken, err := s.manager.GetAccessToken(ctx)
	if err != nil {
		return nil, err
	}

	userData, err := s.manager.Verify(*accessToken)
	if err != nil {
		return nil, err
	}

	if req.GetPackageId() == 0 {
		return nil, errors.New("Type is required")
	}

	checkPackage, err := s.provider.CheckPackage(ctx, req.PackageId)
	if err != nil {
		return nil, err
	}

	if *checkPackage == 0 {
		return nil, errors.New("Package not found")
	}

	userId, err := strconv.ParseUint(userData.UserId, 10, 64)
	if err != nil {
		return nil, err
	}

	err = s.provider.PurchasePackage(ctx, &pb.UserPremiumList{
		UserId:    userId,
		PackageId: req.PackageId,
	})
	if err != nil {
		return nil, err
	}

	return &pb.DefaultResponse{
		Error:   false,
		Code:    http.StatusOK,
		Message: "Success",
	}, nil
}

func (s *Server) GetUserMaxCount(ctx context.Context, req *pb.DefaultRequest) (*pb.UserMaxCountResponse, error) {
	span, _ := apm.StartSpan(ctx, "PurchasePackage", "process")
	span.Action = "execute"
	defer span.End()

	logger := s.GetLogger()
	logger.Infof("[func: register] Check request %v", req)

	if req.GetUserId() == 0 {
		return nil, errors.New("User id is required")
	}

	maxCount, err := s.provider.GetUserMaxCount(ctx, req.UserId)
	if err != nil {
		return nil, err
	}

	if *maxCount == 0 {
		*maxCount = 10
	}

	return &pb.UserMaxCountResponse{
		Error:    false,
		Code:     http.StatusOK,
		Message:  "Success",
		MaxCount: *maxCount,
	}, nil
}
