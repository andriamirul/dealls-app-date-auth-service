package api

import (
	"context"
	"crypto/md5"
	"crypto/rand"
	"errors"
	"fmt"
	mathRand "math/rand"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"dealls-app-date-auth-service/server/lib/constants"
	pb "dealls-app-date-auth-service/server/pb"

	"go.elastic.co/apm/v2"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *Server) Register(ctx context.Context, req *pb.RegisterRequest) (*pb.DefaultResponse, error) {
	span, _ := apm.StartSpan(ctx, "register", "process")
	span.Action = "execute"
	defer span.End()

	logger := s.GetLogger()
	logger.Infof("[func: register] Check request %v", req)

	if req.GetEmail() == "" {
		return nil, errors.New("Email is required")
	}

	if !IsValidEmail(req.Email) {
		return nil, errors.New("Email format is not valid")
	}

	if req.GetPassword() == "" {
		return nil, errors.New("Password is required")
	}

	if req.GetFirstName() == "" || req.GetLastName() == "" {
		return nil, errors.New("Full name must not be empty")
	}

	if req.GetGender() == "" {
		return nil, errors.New("Gender is required")
	}

	req.Gender = strings.ToLower(req.Gender)
	if req.Gender != constants.FEMALE && req.Gender != constants.MALE {
		return nil, errors.New("Gender must be either male or female")
	}

	if !containsNumber(req.Password) {
		return nil, errors.New("Password must contain atleast one number")
	}

	if !containsSymbol(req.Password) {
		return nil, errors.New("Password must contain atleast one symbol")
	}

	checkEmail, err := s.provider.CheckUser(ctx, req.Email)
	if err != nil {
		return nil, err
	}

	if *checkEmail >= 1 {
		return nil, errors.New("Email already exist")
	}

	generateSalt := generateSalt()
	generatedPassword := generateHashPasswordAndSalt(req.Password, generateSalt)

	userId, err := s.provider.InsertUser(ctx, &pb.User{
		Email:    req.Email,
		Password: generatedPassword,
		Salt:     generateSalt,
	})
	if err != nil {
		return nil, errors.New("Failed when register user")
	}

	_, err = s.provider.InsertUserProfile(ctx, &pb.UserProfile{
		UserId:    *userId,
		FirstName: req.FirstName,
		LastName:  req.LastName,
		Gender:    req.Gender,
		Bio:       req.GetBio(),
	})
	if err != nil {
		return nil, errors.New("Failed when register user")
	}

	return &pb.DefaultResponse{
		Error:   false,
		Code:    http.StatusOK,
		Message: "Success",
	}, nil
}

func (s *Server) Login(ctx context.Context, req *pb.LoginRequest) (*pb.LoginResponse, error) {
	span, _ := apm.StartSpan(ctx, "Login", "process")
	span.Action = "execute"
	defer span.End()

	logger := s.GetLogger()
	logger.Infof("[func: Login] Check request %v", req)

	if req.GetEmail() == "" {
		return nil, errors.New("Email is required")
	}

	if !IsValidEmail(req.Email) {
		return nil, errors.New("Email format is not valid")
	}

	if req.GetPassword() == "" {
		return nil, errors.New("Password is required")
	}

	userData, err := s.provider.GetUserByEmail(ctx, req.Email)
	if err != nil {
		return nil, err
	}

	if userData.GetEmail() == "" {
		return nil, errors.New("Invalid Credential")
	}

	hashPasswordAndSalt := generateHashPasswordAndSalt(req.Password, userData.Salt)

	if hashPasswordAndSalt != userData.Password {
		return nil, errors.New("Invalid Credential")
	}

	// Update password and salt
	generateSalt := generateSalt()
	generatedPassword := generateHashPasswordAndSalt(req.Password, generateSalt)
	userData.Salt = generateSalt
	userData.Password = generatedPassword

	err = s.provider.UpdatePasswordAndSalt(ctx, userData)
	if err != nil {
		return nil, err
	}

	// Generate a random 32-byte token
	token := make([]byte, 32)
	_, err = rand.Read(token)
	if err != nil {
		fmt.Println("Error generating token:", err)
		return nil, status.Errorf(codes.Internal, http.StatusText(http.StatusInternalServerError))
	}

	jwtToken, err := s.manager.Generate(
		strconv.FormatUint(userData.Id, 10),
	)

	if err != nil {
		return nil, err
	}

	return &pb.LoginResponse{
		Error:       false,
		Code:        http.StatusOK,
		Message:     "Success",
		AccessToken: jwtToken.GetAccessToken(),
	}, nil
}

func (s *Server) GetAvailableSwipeProfileList(ctx context.Context, req *pb.AvailableSwipeProfileListRequest) (*pb.AvailableSwipeProfileListResponse, error) {
	span, _ := apm.StartSpan(ctx, "GetAvailableSwipeProfileList", "process")
	span.Action = "execute"
	defer span.End()

	logger := s.GetLogger()
	logger.Infof("[func: Login] Check request %v", req)

	var userDatas []*pb.UserData
	if req.GetLimit() > 0 {
		userProfiles, err := s.provider.GetAvailableSwipeProfile(ctx, req)
		if err != nil {
			return nil, err
		}

		if len(userProfiles) > 0 {
			for _, userProfile := range userProfiles {
				userData := &pb.UserData{
					UserId: userProfile.GetUserId(),
					Name:   userProfile.GetFirstName() + " " + userProfile.GetLastName(),
					Gender: userProfile.GetGender(),
					Bio:    userProfile.GetBio(),
				}
				userDatas = append(userDatas, userData)
			}
		}
	}

	return &pb.AvailableSwipeProfileListResponse{
		Error:    false,
		Code:     http.StatusOK,
		Message:  "Success",
		UserData: userDatas,
	}, nil
}

func (s *Server) CheckUserById(ctx context.Context, req *pb.DefaultRequest) (*pb.DefaultResponse, error) {
	span, _ := apm.StartSpan(ctx, "GetAvailableSwipeProfileList", "process")
	span.Action = "execute"
	defer span.End()

	logger := s.GetLogger()
	logger.Infof("[func: Login] Check request %v", req)

	checkUser, err := s.provider.CheckUserById(ctx, req.UserId)
	if err != nil {
		return nil, err
	}

	if *checkUser == 0 {
		return &pb.DefaultResponse{
			Error:   true,
			Code:    http.StatusNotFound,
			Message: "User not found",
		}, nil
	}

	return &pb.DefaultResponse{
		Error:   false,
		Code:    http.StatusOK,
		Message: "Success",
	}, nil
}

func IsValidEmail(email string) bool {
	var emailRegex = regexp.MustCompile(`^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$`)
	return emailRegex.MatchString(email)
}

func containsNumber(s string) bool {
	for _, char := range s {
		if char >= '0' && char <= '9' {
			return true
		}
	}
	return false
}

func containsSymbol(s string) bool {
	for _, char := range s {
		if char >= '!' && char <= '/' {
			return true
		}
		if char >= ':' && char <= '@' {
			return true
		}
		if char >= '[' && char <= '`' {
			return true
		}
	}
	return false
}

func generateSalt() string {
	var randPassword string
	randPassword = RandomString(2, true, 1)
	randPassword = randPassword + strconv.Itoa(RandomNumber(100, 10000))
	randPassword = randPassword + RandomString(3, false, 2)

	return randPassword
}

func RandomString(size int, lowerCase bool, pil int) string {
	var abjad string
	if pil == 1 {
		abjad = "abcdefghijklmnopqrstuvwyxz"
	} else {
		abjad = "zxywvutsrqponmlkjihgfedcba"
	}

	// Seed based on time now and convert time unix to millisecond
	mathRand.Seed(time.Now().UnixNano() / int64(time.Millisecond))
	var hasil string

	for i := 0; i < size; i++ {
		hasil = hasil + string(abjad[mathRand.Intn(len(abjad))])
	}

	if !lowerCase {
		return strings.ToUpper(hasil)
	}

	return hasil
}

func RandomNumber(min, max int) int {
	result := mathRand.Intn(max-min) + min
	return result
}

func generateHashPasswordAndSalt(password, salt string) string {
	bytePassword := []byte(password)
	md5Password := fmt.Sprintf("%x", md5.Sum(bytePassword))
	md5PasswordAndSalt := md5Password + salt
	bytemd5PasswordAndSalt := []byte(md5PasswordAndSalt)
	return fmt.Sprintf("%x", md5.Sum(bytemd5PasswordAndSalt))
}
