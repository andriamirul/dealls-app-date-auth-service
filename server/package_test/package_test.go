package package_test

import (
	"context"
	"dealls-app-date-auth-service/server/api"
	"dealls-app-date-auth-service/server/config"
	"dealls-app-date-auth-service/server/db"
	servicelogger "dealls-app-date-auth-service/server/lib/service-logger"
	pb "dealls-app-date-auth-service/server/pb"
	svc "dealls-app-date-auth-service/server/services"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc/metadata"
)

var logger *servicelogger.AddonsLogrus

const serviceName = "dealls-app-date-interaction-service-test"

func TestPackageList(t *testing.T) {
	startDBConnection()
	defer closeDBConnections()

	appConfig := config.InitConfig("../../.env")
	mockLogger := servicelogger.New(serviceName, appConfig)
	mockServiceConnection := svc.InitServicesConn("", svc.ServiceOption{
		AuthService: appConfig.AuthService,
	})

	server := api.New(db.New(dbSql), mockServiceConnection, mockLogger, appConfig)

	ctx := context.Background()
	login, err := server.Login(ctx, &pb.LoginRequest{
		Email:    "andriamirul@gmail.com",
		Password: "Coba12345!",
	})
	assert.NoError(t, err)

	var newCtx context.Context
	md, _ := metadata.FromIncomingContext(ctx)
	if md == nil {
		md = metadata.New(map[string]string{})
	}
	md.Append("authorization", "Bearer "+login.AccessToken)
	newCtx = metadata.NewIncomingContext(ctx, md)

	resp, err := server.PackageList(newCtx, &pb.Empty{})
	assert.NoError(t, err)
	assert.NotNil(t, resp)
	assert.False(t, resp.Error)
	assert.Equal(t, uint32(http.StatusOK), resp.Code)
}

func TestPurchasePackage(t *testing.T) {
	startDBConnection()
	defer closeDBConnections()

	appConfig := config.InitConfig("../../.env")
	mockLogger := servicelogger.New(serviceName, appConfig)
	mockServiceConnection := svc.InitServicesConn("", svc.ServiceOption{
		AuthService: appConfig.AuthService,
	})

	server := api.New(db.New(dbSql), mockServiceConnection, mockLogger, appConfig)

	ctx := context.Background()
	login, err := server.Login(ctx, &pb.LoginRequest{
		Email:    "andriamirul@gmail.com",
		Password: "Coba12345!",
	})
	assert.NoError(t, err)

	var newCtx context.Context
	md, _ := metadata.FromIncomingContext(ctx)
	if md == nil {
		md = metadata.New(map[string]string{})
	}
	md.Append("authorization", "Bearer "+login.AccessToken)
	newCtx = metadata.NewIncomingContext(ctx, md)

	resp, err := server.PurchasePackage(newCtx, &pb.PurchasePackageRequest{
		PackageId: 1,
	})
	assert.NoError(t, err)
	assert.NotNil(t, resp)
	assert.False(t, resp.Error)
	assert.Equal(t, uint32(http.StatusOK), resp.Code)
}

func TestGetUserMaxCount(t *testing.T) {
	startDBConnection()
	defer closeDBConnections()

	appConfig := config.InitConfig("../../.env")
	mockLogger := servicelogger.New(serviceName, appConfig)
	mockServiceConnection := svc.InitServicesConn("", svc.ServiceOption{
		AuthService: appConfig.AuthService,
	})

	server := api.New(db.New(dbSql), mockServiceConnection, mockLogger, appConfig)

	ctx := context.Background()
	resp, err := server.GetUserMaxCount(ctx, &pb.DefaultRequest{
		UserId: 1,
	})
	assert.NoError(t, err)
	assert.NotNil(t, resp)
	assert.False(t, resp.Error)
	assert.Equal(t, uint32(http.StatusOK), resp.Code)
}
