package package_test

import (
	"os"
	"strconv"
	"time"

	"dealls-app-date-auth-service/server/config"
	"dealls-app-date-auth-service/server/lib/database"
	databasewrapper "dealls-app-date-auth-service/server/lib/database/wrapper"
)

// DB or Collection
var (
	dbSql *database.DbSql
)

func startDBConnection() {
	initDBMain()
}

func closeDBConnections() {
	closeDBMain()
}

func initDBMain() {
	appConfig := config.InitConfig("../../.env")
	var err error
	maxRetry, convErr := strconv.Atoi(appConfig.DbMaxRetry)
	if convErr != nil {
		maxRetry = 3
	}

	dbTimeout, convErr := strconv.Atoi(appConfig.DbTimeout)
	if convErr != nil {
		dbTimeout = 120
	}

	dbSql = database.InitConnectionDB("postgres", database.Config{
		Host:         appConfig.DbHost,
		Port:         appConfig.DbPort,
		User:         appConfig.DbUser,
		Password:     appConfig.DbPassword,
		DatabaseName: appConfig.DbName,
		SslMode:      appConfig.DbSslmode,
		TimeZone:     appConfig.DbTimezone,
		MaxRetry:     maxRetry,
		Timeout:      time.Duration(dbTimeout) * time.Second,
	}, &databasewrapper.DatabaseWrapper{})

	err = dbSql.Connect()

	if err != nil {
		os.Exit(1)
		return
	}

	dbSql.SetMaxIdleConns(0)
	dbSql.SetMaxOpenConns(100)

}

func closeDBMain() {
	dbSql.ClosePmConnection()
}
