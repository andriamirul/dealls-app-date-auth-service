package db

import (
	"context"
	"database/sql"
	"dealls-app-date-auth-service/server/pb"
	"fmt"
	"log"
	"strings"

	"go.elastic.co/apm/v2"
)

func (p *Provider) CheckUser(ctx context.Context, email string) (*uint64, error) {
	span, _ := apm.StartSpan(ctx, "CheckUser", "db")
	span.Subtype = "postgresql"
	span.Action = "query"
	defer span.End()

	sqlStmt, stmtErr := p.GetDbSql().SqlDb.Prepare("SELECT count(1) AS count FROM users WHERE email = $1")
	if stmtErr != nil {
		log.Fatal(stmtErr)
		return nil, stmtErr
	}

	var count uint64
	execErr := sqlStmt.QueryRow(email).Scan(&count)
	if execErr != nil {
		return nil, execErr
	}

	defer sqlStmt.Close()

	return &count, nil
}

func (p *Provider) InsertUser(ctx context.Context, req *pb.User) (*uint64, error) {
	span, _ := apm.StartSpan(ctx, "InsertUser", "db")
	span.Subtype = "postgresql"
	span.Action = "query"
	defer span.End()

	var id uint64
	query := "INSERT INTO users (email,password,salt) values ($1,$2,$3) RETURNING id"
	stmtErr := p.GetDbSql().SqlDb.QueryRow(query, req.Email, req.Password, req.Salt).Scan(&id)
	if stmtErr != nil {
		log.Fatal(stmtErr)
		return nil, stmtErr
	}

	return &id, nil
}

func (p *Provider) InsertUserProfile(ctx context.Context, req *pb.UserProfile) (*uint64, error) {
	span, _ := apm.StartSpan(ctx, "InsertUserProfile", "db")
	span.Subtype = "postgresql"
	span.Action = "query"
	defer span.End()

	var id uint64
	query := "INSERT INTO user_profiles (user_id,first_name,last_name,gender,bio) values ($1,$2,$3,$4,$5) RETURNING id"
	stmtErr := p.GetDbSql().SqlDb.QueryRow(query, req.UserId, req.FirstName, req.LastName, req.Gender, req.GetBio()).Scan(&id)
	if stmtErr != nil {
		log.Fatal(stmtErr)
		return nil, stmtErr
	}

	return &id, nil
}

func (p *Provider) GetUserByEmail(ctx context.Context, email string) (*pb.User, error) {
	span, _ := apm.StartSpan(ctx, "GetUserByEmail", "db")
	span.Subtype = "postgresql"
	span.Action = "query"
	defer span.End()

	sqlStmt, stmtErr := p.GetDbSql().SqlDb.Prepare("SELECT id,email,password,salt FROM users WHERE email = $1")
	if stmtErr != nil {
		log.Fatal(stmtErr)
		return nil, stmtErr
	}

	var user pb.User
	execErr := sqlStmt.QueryRow(email).Scan(&user.Id, &user.Email, &user.Password, &user.Salt)
	if execErr != nil {
		return nil, execErr
	}

	defer sqlStmt.Close()

	return &user, nil
}

func (p *Provider) UpdatePasswordAndSalt(ctx context.Context, req *pb.User) error {
	span, _ := apm.StartSpan(ctx, "UpdatePasswordAndSalt", "db")
	span.Subtype = "postgresql"
	span.Action = "query"
	defer span.End()

	sqlStmt, stmtErr := p.GetDbSql().SqlDb.Prepare("UPDATE users SET password = $1, salt = $2 WHERE id = $3")
	if stmtErr != nil {
		log.Fatal(stmtErr)
		return stmtErr
	}

	_, execErr := sqlStmt.Exec(
		req.Password,
		req.Salt,
		req.Id,
	)

	if execErr != nil {
		return execErr
	}
	return nil
}

func (p *Provider) GetAvailableSwipeProfile(ctx context.Context, req *pb.AvailableSwipeProfileListRequest) ([]*pb.UserProfile, error) {
	span, _ := apm.StartSpan(ctx, "GetAvailableSwipeProfile", "db")
	span.Subtype = "postgresql"
	span.Action = "query"
	defer span.End()

	query := `SELECT user_id,first_name,last_name,bio,gender FROM user_profiles`

	var args []interface{}
	if len(req.GetUserId()) > 0 {
		placeholders := make([]string, len(req.GetUserId()))
		for i := range req.UserId {
			placeholders[i] = fmt.Sprintf("$%d", i+1)
			args = append(args, req.UserId[i])
		}
		query = fmt.Sprintf("%s WHERE user_id NOT IN (%s)", query, strings.Join(placeholders, ","))
	}

	if req.GetLimit() > 0 {
		query += fmt.Sprintf(" LIMIT $%d", len(args)+1)
		args = append(args, req.GetLimit())
	}

	sqlStmt, stmtErr := p.GetDbSql().SqlDb.Prepare(query)
	if stmtErr != nil {
		log.Fatal(stmtErr)
		return nil, stmtErr
	}
	defer sqlStmt.Close()

	ctxTimeout, ctxCancel := context.WithTimeout(ctx, p.GetDbSql().GetTimeout())
	defer ctxCancel()

	result, err := sqlStmt.QueryContext(ctxTimeout, args...)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	var profiles []*pb.UserProfile
	for result.Next() {
		var profile pb.UserProfile
		if err := result.Scan(&profile.UserId, &profile.FirstName, &profile.LastName, &profile.Bio, &profile.Gender); err != nil {
			log.Fatal(err)
			return nil, err
		}

		profiles = append(profiles, &profile)
	}

	defer sqlStmt.Close()

	return profiles, nil
}

func (p *Provider) CheckUserById(ctx context.Context, userId uint64) (*uint64, error) {
	span, _ := apm.StartSpan(ctx, "CheckPackage", "db")
	span.Subtype = "postgresql"
	span.Action = "query"
	defer span.End()

	sqlStmt, stmtErr := p.GetDbSql().SqlDb.Prepare("SELECT count(1) FROM users WHERE id = $1")
	if stmtErr != nil {
		log.Fatal(stmtErr)
		return nil, stmtErr
	}

	var count uint64
	execErr := sqlStmt.QueryRow(userId).Scan(&count)
	if execErr != nil {
		if execErr == sql.ErrNoRows {
			count := uint64(0)
			return &count, nil
		}
		return nil, execErr
	}

	defer sqlStmt.Close()

	return &count, nil
}
