package db

import (
	"context"
	"database/sql"
	"dealls-app-date-auth-service/server/pb"
	"log"

	"go.elastic.co/apm/v2"
)

func (p *Provider) GetPackageList(ctx context.Context) ([]*pb.PackageDescription, error) {
	span, _ := apm.StartSpan(ctx, "GetPackageList", "db")
	span.Subtype = "postgresql"
	span.Action = "query"
	defer span.End()

	sqlStmt, stmtErr := p.GetDbSql().SqlDb.Prepare(`SELECT id,name,description FROM packages`)
	if stmtErr != nil {
		log.Fatal(stmtErr)
		return nil, stmtErr
	}
	defer sqlStmt.Close()

	ctxTimeout, ctxCancel := context.WithTimeout(ctx, p.GetDbSql().GetTimeout())
	defer ctxCancel()

	result, err := sqlStmt.QueryContext(ctxTimeout)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	var packages []*pb.PackageDescription
	for result.Next() {
		var packageItem pb.PackageDescription
		if err := result.Scan(&packageItem.Id, &packageItem.Name, &packageItem.Description); err != nil {
			log.Fatal(err)
			return nil, err
		}

		packages = append(packages, &packageItem)
	}

	defer sqlStmt.Close()

	return packages, nil
}

func (p *Provider) CheckPackage(ctx context.Context, id uint64) (*uint64, error) {
	span, _ := apm.StartSpan(ctx, "CheckPackage", "db")
	span.Subtype = "postgresql"
	span.Action = "query"
	defer span.End()

	sqlStmt, stmtErr := p.GetDbSql().SqlDb.Prepare("SELECT count(1) AS count FROM packages WHERE id = $1")
	if stmtErr != nil {
		log.Fatal(stmtErr)
		return nil, stmtErr
	}

	var count uint64
	execErr := sqlStmt.QueryRow(id).Scan(&count)
	if execErr != nil {
		return nil, execErr
	}

	defer sqlStmt.Close()

	return &count, nil
}

func (p *Provider) PurchasePackage(ctx context.Context, req *pb.UserPremiumList) error {
	span, _ := apm.StartSpan(ctx, "PurchasePackage", "db")
	span.Subtype = "postgresql"
	span.Action = "query"
	defer span.End()

	query := `
		INSERT INTO user_premium_list (user_id, package_id)
		VALUES ($1, $2)
		ON CONFLICT (user_id)
		DO UPDATE SET package_id = EXCLUDED.package_id`

	_, err := p.GetDbSql().SqlDb.Exec(query, req.UserId, req.PackageId)
	if err != nil {
		log.Fatal(err)
		return err
	}

	return nil
}

func (p *Provider) GetUserMaxCount(ctx context.Context, userId uint64) (*uint64, error) {
	span, _ := apm.StartSpan(ctx, "CheckPackage", "db")
	span.Subtype = "postgresql"
	span.Action = "query"
	defer span.End()

	sqlStmt, stmtErr := p.GetDbSql().SqlDb.Prepare("SELECT packages.max_action FROM user_premium_list JOIN packages ON packages.id = user_premium_list.package_id WHERE user_premium_list.user_id = $1")
	if stmtErr != nil {
		log.Fatal(stmtErr)
		return nil, stmtErr
	}

	var count uint64
	execErr := sqlStmt.QueryRow(userId).Scan(&count)
	if execErr != nil {
		if execErr == sql.ErrNoRows {
			count := uint64(0)
			return &count, nil
		}
		return nil, execErr
	}

	defer sqlStmt.Close()

	return &count, nil
}
