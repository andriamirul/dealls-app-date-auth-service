package api_test

import (
	"context"
	"dealls-app-date-auth-service/server/api"
	"dealls-app-date-auth-service/server/config"
	"dealls-app-date-auth-service/server/db"
	servicelogger "dealls-app-date-auth-service/server/lib/service-logger"
	pb "dealls-app-date-auth-service/server/pb"
	svc "dealls-app-date-auth-service/server/services"
	"net/http"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

var logger *servicelogger.AddonsLogrus

const serviceName = "dealls-app-date-interaction-service-test"

func TestLogin(t *testing.T) {
	startDBConnection()
	defer closeDBConnections()

	appConfig := config.InitConfig("../../.env")
	mockLogger := servicelogger.New(serviceName, appConfig)
	mockServiceConnection := svc.InitServicesConn("", svc.ServiceOption{
		AuthService: appConfig.AuthService,
	})

	server := api.New(db.New(dbSql), mockServiceConnection, mockLogger, appConfig)

	ctx := context.Background()
	resp, err := server.Login(ctx, &pb.LoginRequest{
		Email:    "andriamirul@gmail.com",
		Password: "Coba12345!",
	})
	assert.NoError(t, err)
	assert.NotNil(t, resp)
	assert.False(t, resp.Error)
	assert.Equal(t, uint32(http.StatusOK), resp.Code)
}

func TestRegister(t *testing.T) {
	startDBConnection()
	defer closeDBConnections()

	appConfig := config.InitConfig("../../.env")
	mockLogger := servicelogger.New(serviceName, appConfig)
	mockServiceConnection := svc.InitServicesConn("", svc.ServiceOption{
		AuthService: appConfig.AuthService,
	})

	// Create a new server instance
	server := api.New(db.New(dbSql), mockServiceConnection, mockLogger, appConfig)
	logrus.Infof("secret key %v", appConfig.AuthService)

	ctx := context.Background()
	resp, err := server.Register(ctx, &pb.RegisterRequest{
		Email:     "andriamirul+31@gmail.com", //change every test
		Password:  "Coba12345!",
		FirstName: "andri",
		LastName:  "amirul",
		Gender:    "male",
		Bio:       "ini adalah jalan ninjaku",
	})
	assert.NoError(t, err)
	assert.NotNil(t, resp)
	assert.False(t, resp.Error)
	assert.Equal(t, uint32(http.StatusOK), resp.Code)
}

func TestGetAvailableSwipeProfileList(t *testing.T) {
	startDBConnection()
	defer closeDBConnections()

	appConfig := config.InitConfig("../../.env")
	mockLogger := servicelogger.New(serviceName, appConfig)
	mockServiceConnection := svc.InitServicesConn("", svc.ServiceOption{
		AuthService: appConfig.AuthService,
	})

	// Create a new server instance
	server := api.New(db.New(dbSql), mockServiceConnection, mockLogger, appConfig)
	logrus.Infof("secret key %v", appConfig.AuthService)

	ctx := context.Background()
	resp, err := server.GetAvailableSwipeProfileList(ctx, &pb.AvailableSwipeProfileListRequest{
		Limit:  10,
		UserId: []uint64{24},
	})
	assert.NoError(t, err)
	assert.NotNil(t, resp)
	assert.False(t, resp.Error)
	assert.Equal(t, uint32(http.StatusOK), resp.Code)
}
