package config

import (
	"encoding/json"
	"fmt"
	"os"
	"strconv"

	"github.com/joho/godotenv"
)

type Config struct {
	// Listen address is an array of IP addresses and port combinations.
	// Listen address is an array so that this service can listen to many interfaces at once.
	// You can use this value for example: []string{"192.168.1.12:80", "25.49.25.73:80"} to listen to
	// listen to interfaces with IP address of 192.168.1.12 and 25.49.25.73, both on port 80.
	ListenAddress string `config:"LISTEN_ADDRESS"`

	CorsAllowedHeaders []string `config:"CORS_ALLOWED_HEADERS"`
	CorsAllowedMethods []string `config:"CORS_ALLOWED_METHODS"`
	CorsAllowedOrigins []string `config:"CORS_ALLOWED_ORIGINS"`

	AppName string `config:"APP_NAME"`
	AppKey  string `config:"APP_KEY"`

	SwaggerPath string `config:"SWAGGER_PATH"`

	AuthService string `config:"AUTH_SERVICE"`

	DbHost     string `config:"DB_HOST"`
	DbUser     string `config:"DB_USER"`
	DbPassword string `config:"DB_PASSWORD"`
	DbName     string `config:"DB_NAME"`
	DbPort     string `config:"DB_PORT"`
	DbSslmode  string `config:"DB_SSLMODE"`
	DbTimezone string `config:"DB_TIMEZONE"`
	DbMaxRetry string `config:"DB_MAX_RETRY"`
	DbTimeout  string `config:"DB_TIMEOUT"`

	FluentbitHost       string `config:"FLUENTBIT_HOST"`
	FluentbitPort       string `config:"FLUENTBIT_PORT"`
	LoggerTag           string `config:"LOGGER_TAG"`
	LoggerOutput        string `config:"LOGGER_OUTPUT"`
	LoggerLevel         string `config:"LOGGER_LEVEL"`
	TransactionMaxRetry uint32 `config:"TRANSACTION_MAX_RETRY"`
}

func InitConfig(path string) *Config {
	// Todo: add env checker

	if path == "" {
		godotenv.Load(".env")
	} else {
		godotenv.Load(path)
	}

	appName := getEnv("APP_NAME", "")
	if appName == "" {
		appName = getEnv("ELASTIC_APM_SERVICE_NAME", "")
	}

	transactionMaxRetry, err := strconv.Atoi(getEnv("TRANSACTION_MAX_RETRY", "3"))
	if err != nil {
		panic(err)
	}

	return &Config{
		ListenAddress: fmt.Sprintf("%s:%s", os.Getenv("HOST"), os.Getenv("PORT")),
		CorsAllowedHeaders: []string{
			"Connection", "User-Agent", "Referer",
			"Accept", "Accept-Language", "Content-Type",
			"Content-Language", "Content-Disposition", "Origin",
			"Content-Length", "Authorization", "ResponseType",
			"X-Requested-With", "X-Forwarded-For",
		},
		CorsAllowedMethods: []string{"GET", "POST", "PATCH", "DELETE", "PUT"},
		CorsAllowedOrigins: []string{"*"},
		AppName:            appName,
		AppKey:             getEnv("APP_KEY", ""),

		DbHost:     getEnv("DB_HOST", ""),
		DbUser:     getEnv("DB_USER", ""),
		DbPassword: getEnv("DB_PASSWORD", ""),
		DbName:     getEnv("DB_NAME", ""),
		DbPort:     getEnv("DB_PORT", ""),
		DbSslmode:  getEnv("DB_SSLMODE", ""),
		DbTimezone: getEnv("DB_TIMEZONE", ""),
		DbMaxRetry: getEnv("DB_MAX_RETRY", ""),
		DbTimeout:  getEnv("DB_TIMEOUT", ""),

		SwaggerPath: getEnv("SWAGGER_PATH", ""),

		AuthService: getEnv("AUTH_SERVICE", ":9205"),

		FluentbitHost:       getEnv("FLUENTBIT_HOST", ""),
		FluentbitPort:       getEnv("FLUENTBIT_PORT", ""),
		LoggerTag:           getEnv("LOGGER_TAG", ""),
		LoggerOutput:        getEnv("LOGGER_OUTPUT", ""),
		LoggerLevel:         getEnv("LOGGER_LEVEL", ""),
		TransactionMaxRetry: uint32(transactionMaxRetry),
	}
}

func (c *Config) AsString() string {
	data, _ := json.Marshal(c)
	return string(data)
}

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}
