package main

import (
	"os"
	"strconv"
	"time"

	"dealls-app-date-auth-service/server/lib/database"
	databasewrapper "dealls-app-date-auth-service/server/lib/database/wrapper"
)

// DB or Collection
var (
	dbSql *database.DbSql
)

func startDBConnection() {
	logger.Info("Starting Db Connections...")

	initDBMain()
}

func closeDBConnections() {
	closeDBMain()
}

func initDBMain() {
	logger.Info("Main Db - Connecting")
	var err error
	maxRetry, convErr := strconv.Atoi(appConfig.DbMaxRetry)
	if convErr != nil {
		maxRetry = 3
	}

	dbTimeout, convErr := strconv.Atoi(appConfig.DbTimeout)
	if convErr != nil {
		logger.Infof("Failed to convert database Timeout, set to default: %ds", dbTimeout)
		dbTimeout = 120
	}

	dbSql = database.InitConnectionDB("postgres", database.Config{
		Host:         appConfig.DbHost,
		Port:         appConfig.DbPort,
		User:         appConfig.DbUser,
		Password:     appConfig.DbPassword,
		DatabaseName: appConfig.DbName,
		SslMode:      appConfig.DbSslmode,
		TimeZone:     appConfig.DbTimezone,
		MaxRetry:     maxRetry,
		Timeout:      time.Duration(dbTimeout) * time.Second,
	}, &databasewrapper.DatabaseWrapper{})

	err = dbSql.Connect()

	if err != nil {
		logger.Fatalf("Failed connect to DB main: %v", err)
		os.Exit(1)
		return
	}

	dbSql.SetMaxIdleConns(0)
	dbSql.SetMaxOpenConns(100)

	logger.Info("Main Db - Connected")
}

func closeDBMain() {
	logger.Info("Closing DB Main Connection ... ")
	if err := dbSql.ClosePmConnection(); err != nil {
		logger.Fatalf("Error on disconnection with DB Main : %v", err)
	}
	logger.Info("Closing DB Main Success")
}
