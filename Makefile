DOCKERCMD=docker

DOCKER_CONTAINER_NAME?=dealls-app-date-auth-service
DOCKER_CONTAINER_IMAGE?=dealls-app-date-auth-service:latest
DOCKER_BUILD_ARGS?=
DOCKER_DEBIAN_MIRROR?=http://deb.debian.org/debian

BUILD_DATE?=$(shell date -u +'%Y-%m-%dT00:00:00Z')
BUILD_VERSION?=1.0.0

TOPDIR=$(PWD)
BINARY=dealls-app-date-auth-service

.FORCE:
.PHONY: build
.PHONY: vet
.PHONY: unit-test
.PHONY: generates
.PHONY: depend
.PHONY: docker-build
.PHONY: solr
.PHONY: clean
.PHONY: install
.PHONY: install-wkhtmltopdf
.PHONY: all
.PHONY: .FORCE

guard-%:
	@if [ -z '${${*}}' ]; then echo 'Environment variable $* not set' && exit 1; fi

build:
	@echo "Executing go build"
	go build -v -buildmode=pie -ldflags "-X main.version=$(BUILD_VERSION)" -o app ./server/
	@echo "Binary ready"

vet:
	@echo "Running Go static code analysis with go vet"
	go vet -asmdecl -atomic -bool -buildtags -copylocks -httpresponse -loopclosure -lostcancel -methods -nilfunc -printf -rangeloops -shift -structtag -tests -unreachable -unsafeptr ./...
	@echo "go vet complete"

unit-test:
	@echo "Executing go unit test"
	go test -v -cover -json -count=1 -parallel=4 ./...
	@echo "Unit test done"

generate:
	go generate ./...

run:
	go run ./server/ grpc-gw-server --port1 9205 --port2 3025 --grpc-endpoint :9205

migrate-db:
	go run ./server/ db-migrate

depend:
	@echo "Pulling all Go dependencies"
	go mod download
	go mod verify
	go mod tidy
	@echo "You can now run 'make build' to compile all packages"

install-wkhtmltopdf:
	@echo "Install PDF Generator dependencies"
	wget https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/wkhtmltox_0.12.6-1.focal_amd64.deb
	apt install ./wkhtmltox_0.12.6-1.focal_amd64.deb
	@echo "wkhtmltopdf installed"

docker-build:
	$(DOCKERCMD) build -t $(DOCKER_CONTAINER_IMAGE) --build-arg GOPROXY=$(GOPROXY) --build-arg GOSUMDB=$(GOSUMDB) --build-arg BUILD_VERSION=$(BUILD_VERSION) $(DOCKER_BUILD_ARGS) .

default: depend

all: depend install-wkhtmltopdf generate build unit-test

install: depend install-wkhtmltopdf build

clean:
	rm -f $(BINARY)
	rm -f $(BINARY).exe
